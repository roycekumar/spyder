from tkinter import *
root=Tk()
root.title("Multidimensional Arrays")
root.geometry("500x500")
label_1=Label(root)

array_1d=["Ram","Sham","Geeta"]
print(array_1d[1])

array_2d=[["Ram","A"],["Sham","B"],["Geeta","A+"]]
print(array_2d[2][1])

array_3d=[[["Ram","A","Very Good"],["Sham","B","Good"],["Geeta","A+","Execellent"]]]
print(array_3d[0][0][2])

def Report():
    label_1["text"]=array_3d[0][1][0]+" Got grade "+array_3d[0][1][1]+" He is doing "+array_3d[0][1][2]

btn1=Button(root,text="Show Report",command=Report)
btn1.place(relx=0.5,rely=0.5,anchor=CENTER)
label_1.place(relx=0.5,rely=0.6,anchor=CENTER)
root.mainloop()